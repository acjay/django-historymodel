"""
Tests for History Model library
"""

# pylint: disable=R0901,R0904,R0915,C0321

import contextlib
import datetime

from django.conf import settings
from django.core.management import call_command
from django.db import models
from django.test import TestCase, SimpleTestCase

from .models import (
    Now_Time, Beginning_Time, ModelWithContinuousHistory, ModelWithDuration,
    getattr_django_chain, setattr_django_chain)


class TestModelContinuous(ModelWithContinuousHistory):
    """
    Simple model for testing ModelWithContinuousHistory
    """
    _hm_group_by_fields = ('index',)

    time_started = models.DateTimeField(auto_now_add=True)
    index = models.IntegerField()
    data = models.IntegerField()

    class Meta(object):
        app_label = 'historymodel'

    def __repr__(self):
        return u'{0}:{1}'.format(self.index, self.data)


class TestModelDuration(ModelWithDuration):
    """
    Simple model for testing ModelWithContinuousHistory
    """

    _hm_group_by_fields = ('index',)

    time_started = models.DateTimeField(auto_now_add=True)
    time_ended = models.DateTimeField(null=True)
    index = models.IntegerField()
    data = models.IntegerField()

    class Meta(object):
        app_label = 'historymodel'

    def __repr__(self):
        return u'{0}:{1}'.format(self.index, self.data)


class RelatedModel(models.Model):
    """
    Model for testing custom time fields
    """

    time = models.DateTimeField(auto_now_add=True)

    class Meta(object):
        app_label = 'historymodel'


class TestCustomTimeField(ModelWithDuration):
    """
    Model for testing custom time fields
    """

    _hm_time_started_field = 'start__time'
    _hm_time_ended_field = 'end__time'

    start = models.ForeignKey(RelatedModel, related_name='+')
    end = models.ForeignKey(RelatedModel, null=True, related_name='+')

    class Meta(object):
        app_label = 'historymodel'

    def __repr__(self):
        return u'{0}'.format(self.id)


class HistoryTestCaseMixin(object):
    """
    Test case mixin providing a framework for testing sequences of
    events on history model objects.

    History model objects are tied to clock time, which makes them a
    bit tricky to test. This mixin facilitates testing by internally
    storing a sequence of clock times of test setup events as indexed
    "marks", which can then be converted back to actual clock times to
    test for historical consistency.

    When program state is dependent on actual clock time, not just
    event order, clock times can be provided as mark times manually.
    """

    SLEEP_TIME = getattr(settings, 'HISTORY_TEST_SLEEP_TIME', 0.01)

    def _now(self):
        return datetime.datetime.now()

    def _pause(self, seconds):
        import time
        time.sleep(seconds)

    def start_time(self, manualTime=None):
        """
        Initializes timeline.

        Creates Mark 0, which can be used to check the initial state of
        a system.

        :param manualTime: set to a `datetime` to manually provide mark
            time
        """

        # Initialize timeline
        self.sleepTime = self.__class__.SLEEP_TIME
        self.timeline = []

        if manualTime is None:
            self.timeline.append(self._now())
            self._pause(self.sleepTime)
        else:
            self.timeline.append(manualTime)

    def mark_time(self, manualTime=None):
        """
        Creates a timeline mark.

        Call this method in between test setup events that should be
        separated in time. Tests can then to retrieve an actual clock
        time between events to query the history model state at
        specific times by using ``get_time``.

        :param manualTime: set to a `datetime` to manually provide mark
            time
        """

        if manualTime is None:
            self._pause(self.sleepTime)
            self.timeline.append(self._now())
            self._pause(self.sleepTime)
        else:
            self.timeline.append(manualTime)

    def get_time(self, markNumber):
        """
        Retrieves the clock time of a mark index.

        :param markNumber: mark index
        """
        if markNumber is not None:
            return self.timeline[markNumber]
        else:
            return Now_Time

    def end_time(self):
        """
        End the timeline

        (In effect, just adds a slight pause to ensure that clock time
        advances)
        """
        self._pause(self.sleepTime)

    @property
    def replay_marks(self):
        """
        Context manager for designing sequential test cases

        Instead of having to manage times or mark numbers, this
        provides returns an object that manages the sequence of marks,
        and provides a context manager called ``next``, which returns
        Mark objects, containing the clock time and index of each mark
        in turn.  The advantage of using this context manager is that
        it eliminates the need for test cases to encode specific mark
        numbers, which may change as the test case changes.
        """

        # Stash the HistoryTestCaseMixin object for the closures below
        testObj = self

        class Mark(object):
            """
            Simple object containing mark time and index
            """
            def __init__(self, index):
                """
                Encapsulate timeline mark

                :param index: mark index
                """
                self.idx = index

            @property
            def t(self):
                """
                The clock time for this mark
                """
                return testObj.get_time(self.idx)

            def __getitem__(self, offset):
                """
                Get a relative mark time
                """
                return testObj.get_time(self.idx + offset)

        class MarkSequence(object):
            """
            Object for statefully providing Mark objects sequentially
            """
            def __init__(self):
                self.index = 0

            @property
            @contextlib.contextmanager
            def next(self):
                """
                Yield the next Mark object in sequence
                """
                yield Mark(self.index)
                self.index += 1

        return MarkSequence()


class ChainAttrTests(SimpleTestCase):
    def setUp(self):
        class MyObj(object):
            """Dummy object for tests"""
            pass

        self.object1 = MyObj()
        self.object2 = MyObj()
        self.object3 = MyObj()
        self.object4 = MyObj()
        self.object5 = MyObj()
        setattr(self.object1, 'attr', self.object2)
        setattr(self.object2, 'other_attr', self.object3)
        setattr(self.object3, 'yet_another', self.object4)

    def test_single_level_get(self):
        """Test getattr_django_chain for single level"""
        self.assertIs(
            getattr_django_chain(self.object1, 'attr'),
            self.object2)

    def test_multi_level_get(self):
        """Test getattr_django_chain for multiple levels"""
        self.assertIs(
            getattr_django_chain(self.object1, 'attr__other_attr__yet_another'),
            self.object4)

    def test_single_level_set(self):
        """Test setattr_django_chain for single level"""
        setattr_django_chain(self.object1, 'attr', self.object5)
        self.assertIs(
            getattr_django_chain(self.object1, 'attr'),
            self.object5)

    def test_multi_level_set(self):
        """Test setattr_django_chain for multiple levels"""
        setattr_django_chain(
            self.object1, 'attr__other_attr__yet_another', self.object5)
        self.assertIs(
            getattr_django_chain(self.object1, 'attr__other_attr__yet_another'),
            self.object5)


class SingletonTests(TestCase):
    """
    Tests for singletons
    """

    def test_singletons(self):
        """
        Make sure we can't actually instantiate the singletons
        """
        self.assertIsNone(Now_Time())
        self.assertIsNone(Beginning_Time())


class ContinuousHistoryTests(HistoryTestCaseMixin, TestCase):
    """
    Tests for ModelWithContinuousHistory
    """

    def setUp(self):
        super(ContinuousHistoryTests, self).setUp()
        models.register_models('historymodel', TestModelContinuous)
        call_command('syncdb')

        def new_objects(data, model):
            """
            Helper function for creating test items
            :param data: data value
            :param model: model class
            """
            for item_index, data_item in enumerate(data):
                if data_item is not None:
                    model.objects.create(index=item_index, data=data_item)

        # Mark 0
        self.start_time()

        # Mark 1: Create 3 items in the sequence
        self.data1 = [50, 40, 60]
        new_objects(self.data1, TestModelContinuous)
        self.mark_time()

        # Mark 2: Supercede items 1 and 3
        self.data2 = [105, None, 95]
        new_objects(self.data2, TestModelContinuous)
        self.mark_time()

        # Mark 3: Supercede item 2
        self.data3 = [None, 45, None]
        new_objects(self.data3, TestModelContinuous)
        self.mark_time()

    def test_continuous(self):
        """
        Test sequence of events on ModelWithContinuousHistory
        """

        marks = self.replay_marks
        with self.assertNumQueries(1), marks.next as mark:
            self.assertFalse(
                TestModelContinuous.objects.get_all_current(
                    atTime=mark.t).exists())

        with self.assertNumQueries(2), marks.next as mark:
            self.assertQuerysetEqual(
                TestModelContinuous.objects.get_latest_by_group(
                    atTime=mark.t),
                ['0:50', '1:40', '2:60'])
            self.assertEqual(
                TestModelContinuous.objects.get_latest(
                    index=0, atTime=mark.t).data,
                50)

        with self.assertNumQueries(4), marks.next as mark:
            # 4 queries because of time_ended call
            self.assertQuerysetEqual(
                TestModelContinuous.objects.get_latest_by_group(
                    atTime=mark.t),
                ['0:105', '1:40', '2:95'])
            self.assertEqual(
                TestModelContinuous.objects.get_latest(
                    index=0, atTime=mark.t
                ).time_started,
                TestModelContinuous.objects.get_latest(
                    index=0, atTime=self.get_time(mark.idx - 1)
                ).time_ended)

        with self.assertNumQueries(2), marks.next as mark:
            self.assertQuerysetEqual(
                TestModelContinuous.objects.get_latest_by_group(
                    atTime=mark.t),
                ['0:105', '1:45', '2:95'])
            self.assertQuerysetEqual(
                TestModelContinuous.objects.get_latest_by_group(
                    atTime=mark.t, groupByFields=()),
                ['0:50', '1:40', '2:60', '0:105', '2:95', '1:45'],
                ordered=False)

        # Test nonexistent object queries
        self.assertIsNone(TestModelContinuous.objects.get_latest(index=3))

        # Test sequences
        test_item = TestModelContinuous.objects.get_latest(
            index=0, atTime=self.get_time(1))
        self.assertEqual(
            test_item,
            test_item.get_next_term().get_previous_term())
        self.assertIsNone(test_item.get_previous_term())
        self.assertIsNone(test_item.get_next_term().get_next_term())

        # Test modifier
        self.assertIsNone(test_item.get_next_term(
            modifier=lambda x: x.none()))
        self.assertIsNotNone(test_item.get_next_term().get_previous_term(
            modifier=lambda x: x.values()))


class DurationHistoryTests(HistoryTestCaseMixin, TestCase):
    """
    Tests for ModelWithDuration
    """

    def setUp(self):
        super(DurationHistoryTests, self).setUp()
        models.register_models('historymodel', TestModelDuration)
        call_command('syncdb')

        # Mark 0
        self.start_time()

        # Mark 1: Create one item
        self.item1index0 = TestModelDuration.objects.create(index=0, data=50)
        self.mark_time()

        # Mark 2: Create second item
        self.item1index1 = TestModelDuration.objects.create(index=1, data=60)
        self.mark_time()

        # Mark 3: Create item on index=2
        self.item1index2 = TestModelDuration.objects.create(index=2, data=60)
        self.mark_time()

        # Mark 4: End first item
        self.item1index0.end_term()
        self.mark_time()

        # Mark 5: Create parallel item on index=1
        self.item2index1 = TestModelDuration.objects.create(index=1, data=70)
        self.mark_time()

        # Mark 6: End newer item on index=1
        self.item2index1.end_term()
        self.mark_time()

        # Mark 7: Create new parallel item on index=1
        self.item3index1 = TestModelDuration.objects.create(index=1, data=80)
        self.mark_time()

        # Mark 8: End original item on index=1
        self.item1index1.end_term()
        self.mark_time()

        # Mark 9: End all items
        self.item3index1.end_term()
        self.item1index2.end_term()
        self.end_time()

    def test_duration(self):
        """
        Test sequence of events on ModelWithDuration
        """

        # Use the context manager approach

        marks = self.replay_marks
        with marks.next as mark:
            self.assertFalse(
                TestModelDuration.objects.get_all_current(
                    atTime=mark.t).exists())

        with self.assertNumQueries(1), marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['0:50'])

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['0:50', '1:60'])

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['0:50', '1:60', '2:60'])

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['1:60', '2:60'])

        with self.assertNumQueries(1), marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['1:60', '2:60', '1:70'],
                ordered=False)

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['1:60', '2:60'],
                ordered=False)

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['1:60', '2:60', '1:80'],
                ordered=False)

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestModelDuration.objects.get_all_current(atTime=mark.t),
                ['2:60', '1:80'],
                ordered=False)

        self.assertFalse(
            TestModelDuration.objects.get_all_current().exists())


class CustomTimeFieldTest(HistoryTestCaseMixin, TestCase):
    def setUp(self):
        models.register_models('historymodel', TestCustomTimeField)
        models.register_models('historymodel', RelatedModel)
        call_command('syncdb')

    def test_custom_time_field(self):
        """Test custom time field"""

        # Mark 0
        self.start_time()

        # Mark 1: Create one item
        item1 = TestCustomTimeField.objects.create(
            start=RelatedModel.objects.create(
                time=datetime.datetime.now()))
        self.mark_time()

        # Mark 2: Create second item
        item2 = TestCustomTimeField.objects.create(
            start=RelatedModel.objects.create(
                time=datetime.datetime.now()))
        self.mark_time()

        # Mark 3: End first item
        item1.end = RelatedModel.objects.create(
            time=datetime.datetime.now())
        item1.save()
        self.end_time()

        marks = self.replay_marks
        with marks.next as mark:
            self.assertQuerysetEqual(
                TestCustomTimeField.objects.get_all_current(atTime=mark.t),
                [])

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestCustomTimeField.objects.get_all_current(atTime=mark.t),
                [repr(item1)])

        with marks.next as mark:
            self.assertQuerysetEqual(
                TestCustomTimeField.objects.get_all_current(atTime=mark.t),
                [repr(item1), repr(item2)],
                ordered=False)

        self.assertQuerysetEqual(
            TestCustomTimeField.objects.get_all_current(),
            [repr(item2)])

        self.assertTrue(item1.hm_time_ended is not None)
        self.assertLess(item1.hm_time_started, item1.hm_time_ended)
        self.assertTrue(item2.hm_time_ended is None)

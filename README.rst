HistoryModel by Breakrs
=======================

HistoryModel is a library for creating timelined models.

Purpose
-------

HistoryModel contains abstract models and methods for working with
models designed for a create/read-only paradigm.  In this paradigm,
objects are "superceded" instead of updated, and "ended" instead of
deleted.  This creates an immutable history for entity instances and
allows for queries of past states, much like a timeline.  For the
purposes of this documentation, these are referred to as "history
models".

HistoryModel provides two main paradigms: `ModelWithContinuousHistory`_
and `ModelWithDuration`_. It is not currently possible to mix the two
paradigms, but ``ModelWithHistoryBase`` can be subclassed to create new
paradigms, as well.

ModelWithContinuousHistory
~~~~~~~~~~~~~~~~~~~~~~~~~~

In this paradigm, history model objects do not have a explicit end
time. Instead, they form a continuous and gapless timeline, implicitly
ending exactly when their successors begin.

ModelWithDuration
~~~~~~~~~~~~~~~~~

In this paradigm, objects do have explicit end time. This allows the
timeline to have gaps and overlaps between intervals. While this is
more powerful, it requires a bit more care as well. At any given time,
zero, one, or many objects might be in effect, so it is up to the
implementor to ensure data integrity is upheld.

Specific documentation and examples
-----------------------------------

See the source and the tests for inline documentation and examples.

Installation
------------

As long as ``historymodel`` can be imported by your project, there's no
specific need to add it to ``INSTALLED_APPS``. However, it is probably
best to do so anyway, and the tests won't run otherwise.

Metainformation
---------------

Author
......

Alan Johnson

+   alan@breakrs.com
+   http://twitter.com/alanbreakrs
+   http://bitbucket.org/acjohnson55

Copyright and license
.....................

Copyright 2012 Breakrs, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

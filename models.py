"""
Contains abstract models and methods for working with models designed
for a create/read-only paradigm.
"""

import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.db import models


def getattr_chain(obj, attrs):
    """
    Runs getattr iteratively to follow an object graph

    :param obj: root object
    :param attrs: iterable of attributes to dereference
    """
    for attr in attrs:
        obj = getattr(obj, attr, None)
    return obj


def getattr_django_chain(obj, attributePath):
    """
    Gets a nested attribute specified in Django filter related model
    notation

    So: getattr_django_chain(obj, 'attr1__attr2') <-> obj.attr1.attr2

    :param obj: root object
    :param attributePath: Django filter-like attribute string
    """
    return getattr_chain(obj, attributePath.split('__'))


def setattr_django_chain(obj, attributePath, value):
    """
    Sets a nested attribute specified in Django filter related model
    notation.

    So: setattr_django_chain(obj, 'attr1__attr2', val)
        <-> obj.attr1.attr2 = val

    :param obj: root object
    :param attributePath: Django filter-like attribute string
    :param value: value to set
    """
    attrs = attributePath.split('__')
    setattr(getattr_chain(obj, attrs[:-1]), attrs[-1], value)


class classproperty(object):
    """
    Decorator for making class properties
    """
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)


class Now_Time(object):
    """
    Symbolic reference to pass to indicate that a method that works
    with history models should operate on the present, without
    requiring clients of the method to look up the present time
    themselves. Used particularly to allow methods to operate on the
    present by default (as they will in the vast majority of cases)
    but to accept past times as well, without difficulty.
    """
    def __new__(cls, *args, **kwargs):
        return None


class Beginning_Time(object):
    """
    Represents the earliest possible time, analogously to
    :py:class:`Now_Time`
    """
    def __new__(cls, *args, **kwargs):
        return None


class ModelWithHistoryManager(models.Manager):
    """
    Manager that provides time-based query methods to retrieve the
    relevant historical object instances at given times.
    """

    def get_all_current(self, atTime=Now_Time, **kwargs):
        """
        Convenience method to get all current instances

        This is designed to work on QuerySets from models that extend
        :py:class:`ModelWithHistoryBase`-derived classes. This
        generalizes the process of getting the model instance that is
        in effect at any given time, allowing simpler retrieval of past
        snapshots of object state. The default is to retrive the set of
        instances in effect at present.

        When atTime is set to a datetime value, only instances that began
        before that instant in time will be returned.

        :param atTime: query time
        :param kwargs: filter arguments
        """

        if atTime is Now_Time:
            atTime = datetime.datetime.now()

        kwargs.update({self.model._time_started_field + '__lte': atTime})

        # Get all objects that started before atTime
        qs = self.get_query_set().filter(**kwargs)

        return qs


class ModelWithHistoryBase(models.Model):
    """
    Base class for ModelWithContinuousHistory and ModelWithDuration,
    abstract model classes for implementing versioned/timelined
    objects. Classes that extend this class should have at least one
    identifying field that unifies instances that should belong to the
    same timeline.

    In many cases, a history model class will track multiple distinct
    object timelines. To disambiguate each timeline, history model
    classes should define a class attribute called
    :py:attr:`_hm_group_by_fields`, which specifies what model fields
    distinguish timelines of distinct objects.

    The two derived classes have different definitions of `time_ended`.
    See below for more details.
    """

    _hm_group_by_fields = ()

    class DoesNotExist(Exception):
        """
        Thrown if trying to navigate to a term that does not exist
        """
        pass

    class Meta(object):
        abstract = True

    @classproperty
    def _time_started_field(self):
        return getattr(self, '_hm_time_started_field', 'time_started')

    @property
    def hm_time_started(self):
        """
        Gets the start time field as specified by _hm_time_started_field
        """
        return getattr_django_chain(self, self._time_started_field)

    def timeline(self, groupByFields=None):
        """
        Gets the timeline of objects in an objects historical record.

        Given a model object, this method returns the Queryset of all
        model objects that match itself in these fields, most
        typically, all past and future versions of the same object.

        Does nothing for model objects without a
        :py:attr:`_hm_group_by_fields` class attribute if `groupByFields`
        is also unspecified.

        :param groupByFields: fields to match to current object
        """

        if groupByFields is None:
            groupByFields = getattr(
                self.__class__, '_hm_group_by_fields', groupByFields)

        filterDict = dict(
            (fieldName, getattr(self, fieldName))
            for fieldName in groupByFields)

        return self.__class__.objects.filter(**filterDict)

    def get_previous_term(self, groupByFields=None, modifier=None):
        """
        The previous term in the model history

        Returns `None` if no previous term exists.

        :param groupByFields: fields to group on (normally specified in
            model class)
        :param modifier: function to apply before returning object
        """

        tsf = self._time_started_field
        pastTerms = (
            self.timeline(groupByFields).filter(
                **{tsf + '__lt': self.hm_time_started}).order_by('-' + tsf))

        if modifier:
            pastTerms = modifier(pastTerms)

        if pastTerms:
            return pastTerms[0]
        else:
            return None

    def get_next_term(self, groupByFields=None, modifier=None):
        """
        The next term in the model history

        Returns `None` if no previous term exists.

        :param groupByFields: fields to group on (normally specified in
            model class)
        :param modifier: function to apply before returning object
        """

        tsf = self._time_started_field
        futureTerms = self.timeline(groupByFields).filter(
            **{tsf + '__gt': self.hm_time_started}).order_by(tsf)

        if modifier:
            futureTerms = modifier(futureTerms)

        if futureTerms:
            return futureTerms[0]
        else:
            return None


class ModelWithContinuousHistoryManager(ModelWithHistoryManager):
    """
    Methods for retrieving history model objects with continuous
    timelines
    """

    def get_latest(self, atTime=Now_Time, **kwargs):
        """
        Get the latest valid instance of a continuous history model

        This is used to query instantaneous values of model objects at
        the present time and at past instants.  Callers should check
        for None's and raise appropriate exceptions.

        :param atTime: query time
        :param kwargs: filter arguments
        """

        try:
            qs = self.get_all_current(atTime=atTime, **kwargs)
            return qs.latest(self.model._time_started_field)

        except (ObjectDoesNotExist, IndexError):
            return None

    def get_latest_by_group(
            self, atTime=Now_Time, groupByFields=None, **kwargs):
        """
        Gets the latest valid instances of continuous history models,
        grouped on specified fields.

        By default, this will group on the fields specified in the
        tuple of field names assigned to the subclass's
        :py:attr:`_hm_group_by_fields` class attribute.

        :param atTime: query time
        :param groupByFields: fields to group models on
        :param kwargs: filter arguments
        """

        if groupByFields is None:
            groupByFields = getattr(
                self.model, '_hm_group_by_fields', groupByFields)

        qs = self.get_all_current(atTime=atTime, **kwargs)
        qs = qs.order_by(
            *(groupByFields + ('-' + self.model._time_started_field,)))
        return qs.distinct(*groupByFields)


class ModelWithContinuousHistory(ModelWithHistoryBase):
    """
    Represents a model with continuous history

    Concrete models should extend this interface when it is desired to
    have the ability to query past snapshots of application state.
    These queries can be done with the get_all_current and get_latest
    convenience methods.

    ModelWithContinuousHistory differs from
    :py:class:`ModelWithDuration` in that this class models objects
    that, once initialized, exist continuously and do not overlap (like
    parameter values).

    Models that implement multiple parallel continuities,
    differentiated by some of their fields should define
    :py:attr:`_hm_group_by_fields` to be a tuple of names of those fields.

    """

    objects = ModelWithContinuousHistoryManager()

    @property
    def time_ended(self):
        """
        The shared time when this entry ends and the next one begins

        *Avoid calling this when efficiency is critical*; it triggers
        a DB query.

        Returns None if this is the latest segment
        """

        next_term = self.get_next_term()
        return next_term.hm_time_started if next_term else None

    class Meta(object):
        abstract = True


class ModelWithDurationManager(ModelWithHistoryManager):
    """
    Methods for retrieving history model objects with durations by time
    """

    use_for_related_fields = True

    def get_all_current(self, atTime=Now_Time, **kwargs):
        """
        Gets all objects that have begun but not ended at the current time

        :param atTime: query time
        :param kwargs: filter arguments
        """

        if atTime is Now_Time:
            atTime = datetime.datetime.now()

        qs = super(ModelWithDurationManager, self).get_all_current(
            atTime=atTime, **kwargs)

        tef = self.model._time_ended_field
        return qs.exclude(**{tef + '__lt': atTime})


class ModelWithDuration(ModelWithHistoryBase):
    """
    Represent a model with durations of existence

    See :py:class:`ModelWithContinuousHistory`.

    ModelWithDuration differs from ModelWithContinuousHistory in that
    the former models variables that have finite durations, which may
    have gaps and overlaps. DO NOT assume that durations do not have
    gaps or overlaps, because strange bugs could occur if this
    assumption does not hold, and it is tricky to ensure data
    consitency.
    """

    objects = ModelWithDurationManager()

    class Meta(object):
        abstract = True

    @classproperty
    def _time_ended_field(self):
        return getattr(self, '_hm_time_ended_field', 'time_ended')

    @property
    def hm_time_ended(self):
        """
        Gets the end time field as specified by _hm_time_ended_field
        """
        return getattr_django_chain(self, self._time_ended_field)

    @hm_time_ended.setter
    def hm_time_ended(self, value):
        """
        Sets the end time field as specified by _hm_time_ended_field

        :param value: end time
        """
        setattr_django_chain(self, self._time_ended_field, value)

    def end_term(self):
        """
        Ends this time span
        """

        self.hm_time_ended = datetime.datetime.now()
        self.save()